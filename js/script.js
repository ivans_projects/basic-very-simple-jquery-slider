$(function(){
  $(".slides__list > li:gt(0)").hide();
  setInterval(function(){
    $('.slides__list > li:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('.slides__list');
  },  5000);
});
